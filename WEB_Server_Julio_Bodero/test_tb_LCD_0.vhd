library ieee;
library std;
use ieee.std_logic_1164.all;
use std.textio.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
USE IEEE.STD_LOGIC_TEXTIO.ALL;
entity test_tb_LCD_0 is
end test_tb_LCD_0;

architecture sol of test_tb_LCD_0 is
	component lcd_controller2 is
	port (
		clk        : IN    STD_LOGIC;  --system clock
      reset_n    : IN    STD_LOGIC;  --active low reinitializes lcd
      rw, rs, e  : OUT   STD_LOGIC;  --read/write, setup/data, and enable for lcd
      lcd_data   : OUT   STD_LOGIC_VECTOR(7 DOWNTO 0); --data signals for lcd
	   line1_buffer_in : IN STD_LOGIC_VECTOR(127 downto 0); -- Data for the top line of the LCD
	   line2_buffer_in : IN STD_LOGIC_VECTOR(127 downto 0);
	   line1_buffer_out : OUT STD_LOGIC_VECTOR(127 downto 0); -- Data for the top line of the LCD
	   line2_buffer_out : OUT STD_LOGIC_VECTOR(127 downto 0)); -- Data for the bottom line of the LCD
	end component lcd_controller2;
	
	
	--Señales a utilizar
	type vect_bool is array(natural range <>) of boolean;
	constant SEPARADOR : string(1 to 2):=", ";
	constant half_period: time := 5 ns;
	signal clock: std_logic;
	signal reset: std_logic:='1';
	signal lcd_e: std_logic:='0';
	signal lcd_rs: std_logic:='0';
	signal lcd_rw: std_logic:='0';
	signal top_line_in: std_logic_vector(127 downto 0):=(others =>'0');
	signal top_line_out: std_logic_vector(127 downto 0):=(others =>'0');
	signal bottom_line_in: std_logic_vector(127 downto 0):=(others =>'0');
	signal bottom_line_out: std_logic_vector(127 downto 0):=(others =>'0');
	signal lcd_db: std_logic_vector(7 downto 0):=(others =>'0');
	type arreglo1 is array (0 to 3, 0 to 1) of String (1 to 20);
	type arreglo2 is array (0 to 3, 0 to 1) of std_logic_vector(127 downto 0);
	signal datos1: arreglo2;
	constant l1a: std_logic_vector(127 downto 0):=x"50616973202020202020202020202020";constant l1b: std_logic_vector(127 downto 0):=x"49736c61202020202020202020202020";
	constant l2a: std_logic_vector(127 downto 0):=x"43697564616420202020202020202020";constant l2b: std_logic_vector(127 downto 0):=x"43697564616420202020202020202020";
	constant l3a: std_logic_vector(127 downto 0):=x"4361706974616c202020202020202020";constant l3b: std_logic_vector(127 downto 0):=x"52656c69657665202020202020202020";
	constant l4a: std_logic_vector(127 downto 0):=x"566f6c63616e20202020202020202020";constant l4b: std_logic_vector(127 downto 0):=x"446573696572746f2020202020202020";
begin
	--Instanciando bloque LCD controller
	dut: lcd_controller2 
		port map(clock,reset,lcd_rw,lcd_rs,lcd_e,lcd_db,top_line_in,bottom_line_in,top_line_out,bottom_line_out);
	--Generando señal de reloj
	clock_generator: process
	begin
      for i in 0 to 2000000 loop
         clock <= '0';
         wait for half_period;
         clock <= '1';
         wait for half_period;
     end loop;  -- i
	end process clock_generator;
	datos1(0,0)<=l1a; datos1(0,1)<=l1b;
	datos1(1,0)<=l2a; datos1(1,1)<=l2b;
	datos1(2,0)<=l3a; datos1(2,1)<=l3b;
	datos1(3,0)<=l4a; datos1(3,1)<=l4b;
	
	input_control: process
	variable status: file_open_status;
	variable l_w:line;
	variable asd: string(1 to 20);
	file f_w: text;	
	begin
	file_open(status,f_w,"resultados.txt",write_mode);
	reset<='0';
	lcd_e<='1';
	lcd_rs<='0';
	lcd_rw<='0';
	wait for 2*half_period*1000;
	for i in 0 to 3 loop
		assert ((datos1(i,0)'length > 128) or (datos1(i,1)'length > 128)) report "VALORES LEIDOS CORRECTAMENTE" severity note;
		assert not((datos1(i,0)'length > 128) or (datos1(i,1)'length > 128)) report "VALORES LEIDOS INCORRECTAMENTE" severity error;
		
		top_line_in<=datos1(i,0);
		bottom_line_in<=datos1(i,1);
		wait for 2*half_period*3000;
		write(l_w,datos1(i,0),right,20);
		write(l_w,SEPARADOR);
		write(l_w,datos1(i,1),right,20);
		writeline(f_w,l_w);
	end loop;
	file_close(f_w);
	--Finalizando simulación
	assert  false	report "Simulacion Completada"
						severity failure;
	end process input_control;

end sol;