/*
 * "Hello World" example.
 *
 * This example prints 'Hello from Nios II' to the STDOUT stream. It runs on
 * the Nios II 'standard', 'full_featured', 'fast', and 'low_cost' example
 * designs. It runs with or without the MicroC/OS-II RTOS and requires a STDOUT
 * device in your system's hardware.
 * The memory footprint of this hosted application is ~69 kbytes by default
 * using the standard reference design.
 *
 * For a reduced footprint version of this template, and an explanation of how
 * to reduce the memory footprint for a given application, see the
 * "small_hello_world" template.
 *
 */

#include <stdio.h>

int main()
{

//	unsigned int suma = 0x04000000;
//	unsigned int *var = suma;
//	int ceros = 0;
//	while (1){
//		if(*var == 0){
//			ceros++;
//		}
//
//		suma+=1;
//		var = suma;
//		if(var == 0x07ffffff){
//			break;
//		}
//
//	}
	printf("Fin Analisis\n");

  return 0;
}
