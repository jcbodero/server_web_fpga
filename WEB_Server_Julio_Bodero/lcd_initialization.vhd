LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;
use ieee.numeric_std.all;

ENTITY lcd_initialization IS
	GENERIC(
		CURSOR_ON: std_logic:='1';
		BLINKING_ON: std_logic:='1';
		CLOCK_CYCLES_FOR_15MS: integer:=750000;
		W15: integer:=20;
		COUNTER_INCREMENT_FOR_15MS: std_logic_vector(19 downto 0):="00000000000000000001";
		CLOCK_CYCLES_FOR_5MS: integer:=250000;
		W5: integer:=18;
		COUNTER_INCREMENT_FOR_5MS: std_logic_vector(17 downto 0):="000000000000000001");
  PORT(
    clk        : IN    STD_LOGIC;
    reset    : IN    STD_LOGIC;
	 initialize_LCD_display, command_was_sent: in std_logic;
	 done_initialization, send_command: out std_logic;
	 the_command: out std_logic_vector(8 downto 0));
END lcd_initialization;

ARCHITECTURE controller OF lcd_initialization IS
	CONSTANT LCD_INIT_STATE_0_WAIT_POWER_UP: std_logic_vector(1 downto 0):="00";
	CONSTANT LCD_INIT_STATE_1_SEND_COMMAND: std_logic_vector(1 downto 0):="01";
	CONSTANT LCD_INIT_STATE_2_CHECK_DONE: std_logic_vector(1 downto 0):="10";
	CONSTANT LCD_INIT_STATE_3_DONE: std_logic_vector(1 downto 0):="11";
	CONSTANT AUTO_INIT_LENGTH: std_logic_vector(3 downto 0):="1000";
	SIGNAL command_rom: std_logic_vector(8 downto 0);
	SIGNAL waiting_power_up: std_logic_vector(W15 downto 1);
	SIGNAL waiting_to_send: std_logic_vector(W5 downto 1);
	SIGNAL command_counter: std_logic_vector(3 downto 0);
	SIGNAL ns_lcd_initialize: std_logic_vector(1 downto 0);
	SIGNAL s_lcd_initialize: std_logic_vector(1 downto 0);
	SIGNAL done_initialization_x1: std_logic;
	SIGNAL send_command_x2: std_logic;
	SIGNAL the_command_x3: std_logic_vector(8 downto 0);
BEGIN
	done_initialization<=done_initialization_x1;
	send_command<=send_command_x2;
	the_command<=the_command_x3;
	process
	begin
	wait until(clk'event and clk='1');
	if (reset='1') then s_lcd_initialize<=LCD_INIT_STATE_0_WAIT_POWER_UP;
	else s_lcd_initialize<=ns_lcd_initialize; end if;
	end process;
	
	process(command_was_sent, waiting_power_up, ns_lcd_initialize, initialize_LCD_display, command_counter,
				s_lcd_initialize, clk, reset, waiting_to_send)
	begin
	ns_lcd_initialize<=LCD_INIT_STATE_0_WAIT_POWER_UP;
	CASE s_lcd_initialize is
		when LCD_INIT_STATE_0_WAIT_POWER_UP=>if (((waiting_power_up=std_logic_vector(to_unsigned(CLOCK_CYCLES_FOR_15MS, 20))))
														and initialize_LCD_display='1') then ns_lcd_initialize<=LCD_INIT_STATE_1_SEND_COMMAND;
														else ns_lcd_initialize<=LCD_INIT_STATE_0_WAIT_POWER_UP; end if;
		when LCD_INIT_STATE_1_SEND_COMMAND=> if (command_was_sent='1') then ns_lcd_initialize<=LCD_INIT_STATE_2_CHECK_DONE;
														else ns_lcd_initialize<=LCD_INIT_STATE_1_SEND_COMMAND; end if;
		when LCD_INIT_STATE_2_CHECK_DONE=> if (command_counter=AUTO_INIT_LENGTH) then ns_lcd_initialize<=LCD_INIT_STATE_3_DONE;
														else if(waiting_to_send=std_logic_vector(to_unsigned(CLOCK_CYCLES_FOR_5MS, 18))) then
														ns_lcd_initialize<=LCD_INIT_STATE_1_SEND_COMMAND; else
														ns_lcd_initialize<=LCD_INIT_STATE_2_CHECK_DONE; end if; end if;
		when LCD_INIT_STATE_3_DONE=> if(initialize_LCD_display='1') then ns_lcd_initialize<=LCD_INIT_STATE_3_DONE;
												else ns_lcd_initialize<=LCD_INIT_STATE_0_WAIT_POWER_UP; end if;
		when others=>ns_lcd_initialize<=LCD_INIT_STATE_0_WAIT_POWER_UP;
	end case;
	end process;
	
	process
	begin
	wait until(clk'event and clk='1');
	if (reset='1') then
		the_command_x3<="000000000";
	else the_command_x3<=command_rom; end if;
	end process;
	
	process
	begin
	wait until(clk'event and clk='1');
	if (reset='1') then
		waiting_power_up<=(OTHERS=>'0');
	else if((s_lcd_initialize=LCD_INIT_STATE_0_WAIT_POWER_UP) and
				not(waiting_power_up=std_logic_vector(to_unsigned(CLOCK_CYCLES_FOR_15MS, 20)))) then
				waiting_power_up<=waiting_power_up + COUNTER_INCREMENT_FOR_15MS;
	end if; end if;
	end process;
	
	process
	begin
	wait until(clk'event and clk='1');
	if(reset='1') then waiting_to_send<=(OTHERS=>'0');
	else if(s_lcd_initialize=LCD_INIT_STATE_2_CHECK_DONE) then
			if (not(waiting_to_send=std_logic_vector(to_unsigned(CLOCK_CYCLES_FOR_5MS, 18)))) then
				waiting_to_send<=waiting_to_send+COUNTER_INCREMENT_FOR_5MS;end if;
			else waiting_to_send<=(OTHERS=>'0'); end if; end if;
	end process;
	
	process
	begin
	wait until(clk'event and clk='1');
	if (reset='1') then command_counter<="0000";
	else if(s_lcd_initialize=LCD_INIT_STATE_1_SEND_COMMAND) then
		if (command_was_sent='1') then
			command_counter<=command_counter+"0001";
		end if;
		else if(s_lcd_initialize=LCD_INIT_STATE_3_DONE) then
			command_counter<="0101"; end if; end if; end if;
	end process;
	send_command_x2<='1' when s_lcd_initialize=LCD_INIT_STATE_1_SEND_COMMAND else '0';
	done_initialization_x1<='1' when s_lcd_initialize=LCD_INIT_STATE_3_DONE else '0';
	process (command_was_sent, command_rom, waiting_power_up, command_counter, s_lcd_initialize,clk,reset)
	begin
		case command_counter is
		when "0000" => command_rom <="000110000";
		when "0001" => command_rom <="000110000";
		when "0010" => command_rom <="000110000";
		when "0011" => command_rom <="000111100";
		when "0100" => command_rom <="000001000";
		when "0101" => command_rom <="000000001";
		when "0110" => command_rom <="000000110";
		when "0111" => command_rom <="0000011"&CURSOR_ON&BLINKING_ON;
		when OTHERS => command_rom <="000000000";
		end case;
	end process;
END controller;
