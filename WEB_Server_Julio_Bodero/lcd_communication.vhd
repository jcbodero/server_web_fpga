LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;
use ieee.numeric_std.all;

ENTITY lcd_communication IS
	GENERIC(CLOCK_CYCLES_FOR_IDLE_STATE: std_logic_vector(6 downto 0):="1111111";
				IC: integer:=7;
				IDLE_COUNTER_INCREMENT: std_logic_vector(6 downto 0):="0000001";
				CLOCK_CYCLES_FOR_OPERATION_STATE:integer:=3;
				CLOCK_CYCLES_FOR_ENABLE_STATE:integer:=15;
				CLOCK_CYCLES_FOR_HOLD_STATE:integer:=1;
				SC: integer:=4;
				COUNTER_INCREMENT: std_logic_vector(3 downto 0):="0001");

  PORT(
    clk        : IN    STD_LOGIC;
    reset    : IN    STD_LOGIC;
	 data_in: in std_logic_vector(7 downto 0);
	 enable,rs,rw,display_on,back_light_on: in std_logic;
	 LCD_DATA:inout std_logic_vector(7 downto 0);
	 LCD_ON,LCD_BLON,LCD_EN,LCD_RS,LCD_RW,transfer_complete:out std_logic;
	 data_out : out std_logic_vector(7 downto 0));	 
END lcd_communication;

ARCHITECTURE controller OF lcd_communication IS
	CONSTANT LCD_STATE_4_IDLE: std_logic_vector(2 downto 0):="100";
	CONSTANT LCD_STATE_0_OPERATION: std_logic_vector(2 downto 0):="000";
	CONSTANT LCD_STATE_1_ENABLE: std_logic_vector(2 downto 0):="001";
	CONSTANT LCD_STATE_2_HOLD: std_logic_vector(2 downto 0):="010";
	CONSTANT LCD_STATE_3_END: std_logic_vector(2 downto 0):="011";
	SIGNAL data_to_lcd: std_logic_vector(7 downto 0);
	SIGNAL idle_counter: std_logic_vector(IC downto 1);
	SIGNAL state_0_counter: std_logic_vector(SC downto 1);
	SIGNAL state_1_counter: std_logic_vector(SC downto 1);
	SIGNAL state_2_counter: std_logic_vector(SC downto 1);
	SIGNAL ns_lcd: std_logic_vector(2 downto 0);
	SIGNAL s_lcd: std_logic_vector(2 downto 0);
	SIGNAL temp_x8: std_logic_vector(7 downto 0);
	SIGNAL lcd_on_x1: std_logic;
	SIGNAL lcd_blon_x2: std_logic;
	SIGNAL lcd_en_x3: std_logic;
	SIGNAL lcd_rs_x4: std_logic;
	SIGNAL lcd_rw_x5: std_logic;
	SIGNAL data_out_x6: std_logic_vector(7 downto 0);
	SIGNAL transfer_complete_x7: std_logic;
BEGIN
	LCD_ON<=LCD_ON_x1;
	LCD_BLON<=LCD_BLON_x2;
	LCD_EN<=LCD_EN_x3;
	LCD_RS<=LCD_RS_x4;
	LCD_RW<=LCD_RW_x5;
	data_out<=data_out_x6;
	transfer_complete<=transfer_complete_x7;
	process
	begin
		wait until(clk'event and clk='1');
		if(reset='1') then
			s_lcd<=LCD_STATE_4_IDLE;
		else s_lcd<=ns_lcd; end if;
	end process;
	process(idle_counter,state_1_counter,state_2_counter, ns_lcd, clk, reset, s_lcd, enable)
	begin
		ns_lcd<=LCD_STATE_4_IDLE;
		CASE s_lcd is
		when LCD_STATE_4_IDLE=>if ((enable='1') and (idle_counter=CLOCK_CYCLES_FOR_IDLE_STATE)) then
										ns_lcd<=LCD_STATE_0_OPERATION;
										else ns_lcd<=LCD_STATE_4_IDLE; end if;
		when LCD_STATE_0_OPERATION=>if (state_0_counter = std_logic_vector(to_unsigned(CLOCK_CYCLES_FOR_OPERATION_STATE, 4))) then
										ns_lcd<=LCD_STATE_1_ENABLE;
										else ns_lcd<=LCD_STATE_0_OPERATION; end if;
		when LCD_STATE_1_ENABLE=>if(state_1_counter=std_logic_vector(to_unsigned(CLOCK_CYCLES_FOR_ENABLE_STATE, 4))) then
										ns_lcd<=LCD_STATE_2_HOLD;
										else ns_lcd<=LCD_STATE_1_ENABLE; end if;
		when LCD_STATE_2_HOLD=>if(state_2_counter=std_logic_vector(to_unsigned(CLOCK_CYCLES_FOR_HOLD_STATE, 4))) then
										ns_lcd<=LCD_STATE_3_END;
										else ns_lcd<=LCD_STATE_2_HOLD; end if;
		when LCD_STATE_3_END=> if(enable='0') then ns_lcd<=LCD_STATE_4_IDLE;
										else ns_lcd<=LCD_STATE_3_END; end if;
		when OTHERS=> ns_lcd<=LCD_STATE_4_IDLE;
		end case;
	end process;
	
	process
	begin
	wait until(clk'event and clk='1');
		if(reset='1') then
			LCD_ON_x1<='0';
			LCD_BLON_x2<='0';
		else LCD_ON_x1<=display_on; LCD_BLON_x2<=back_light_on; end if;
	end process;
	
	process
	begin
	wait until(clk'event and clk='1');
		if(reset='1') then
		LCD_EN_x3<='0';
		LCD_RS_x4<='0';
		LCD_RW_x5<='0';
		data_out_x6<="00000000";
		transfer_complete_x7<='0';
		else if(s_lcd=LCD_STATE_1_ENABLE) then LCD_EN_x3<='1';
				else LCD_EN_x3<='0'; end if;
				if (s_lcd=LCD_STATE_4_IDLE) then LCD_RS_x4<=rs; LCD_RW_x5<=rw; data_to_lcd<=data_in; end if;
				if (s_lcd=LCD_STATE_1_ENABLE) then data_out_x6<=LCD_DATA; end if;
				if (s_lcd=LCD_STATE_3_END) then transfer_complete_x7<='1';
				else transfer_complete_x7<='0'; end if; end if;
		end process;
	process
	begin
	wait until(clk'event and clk='1');
	if(reset='1') then
		idle_counter<=(OTHERS=>'0');
	else if(s_lcd=LCD_STATE_4_IDLE) then
		idle_counter<=idle_counter+IDLE_COUNTER_INCREMENT;
		else idle_counter<=(OTHERS=>'0'); end if; end if;
	end process;
	
	process
	begin
	wait until(clk'event and clk='1');
	if(reset='1') then state_0_counter<=(OTHERS=>'0');
							state_1_counter<=(OTHERS=>'0');
							state_2_counter<=(OTHERS=>'0');
	else if(s_lcd=LCD_STATE_0_OPERATION) then state_0_counter<=state_0_counter+COUNTER_INCREMENT;
			else state_0_counter<=(OTHERS=>'0'); end if;
			if(s_lcd=LCD_STATE_1_ENABLE) then state_1_counter<=state_1_counter+COUNTER_INCREMENT;
			else state_1_counter<=(OTHERS=>'0'); end if;
			if(s_lcd=LCD_STATE_2_HOLD) then state_2_counter<=state_2_counter+COUNTER_INCREMENT;
			else state_2_counter<=(OTHERS=>'0'); end if; end if;
	end process;
	temp_x8<=data_to_lcd when (((s_lcd=LCD_STATE_1_ENABLE) or (s_lcd=LCD_STATE_2_HOLD))
										and (LCD_RW_x5='0')) else "ZZZZZZZZ";
	LCD_DATA<=temp_x8;
END controller;
