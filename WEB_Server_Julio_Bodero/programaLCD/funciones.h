
#ifndef __FUNCIONES_H__
#define __FUNCIONES_H__

unsigned char get_DDRAM_addr(unsigned x_pos, unsigned y_pos);
void lcdEscirbir (alt_dev *base, const char *cadena);
void lcdSendCmd(alt_dev *base, unsigned char cmd);
int lcdSetCursor(alt_dev *base , unsigned x_pos, unsigned y_pos);
int lcdBorrarPos(alt_dev *base, unsigned x_pos, unsigned y_pos);
void lcdBorrarTodo(alt_dev *base);



#endif /* __ALTERA_UP_AVALON_CHARACTER_LCD_H__ */
