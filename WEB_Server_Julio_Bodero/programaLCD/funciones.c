#include "altera_up_avalon_character_lcd_regs.h"
#include "unistd.h"
#include <string.h>

unsigned char get_DDRAM_addr(unsigned x_pos, unsigned y_pos)
{
	//assume valid inputs
	unsigned char addr = 0x00000000;
	if (y_pos == 0)
	{
		addr |= x_pos;
	}
	else
	{
		addr |= x_pos;
		addr |= 0x00000040;
	}
	// b_7 is always 1 for DDRAM address, see datasheet
	return (addr | 0x00000080);
}

void lcdEscirbir (alt_dev *base, const char *cadena){
	unsigned int i;
	printf("%d",strlen(cadena) );
	for (i = 0; i < strlen(cadena); i++)
			{
				IOWR_ALT_UP_CHARACTER_LCD_DATA(base, *(cadena+i));
				usleep(500);
			}
}
void lcdSendCmd(alt_dev *base, unsigned char cmd)
{
 	// NOTE: We use the term Instruction Register and Control Register interchangeably
	IOWR_ALT_UP_CHARACTER_LCD_COMMAND(base, cmd);
}
int lcdSetCursor(alt_dev *base , unsigned x_pos, unsigned y_pos)
{
	//boundary check
	if (x_pos > 39 || y_pos > 1 )
		// invalid argument
		return -1;
	// calculate address
	unsigned char addr = get_DDRAM_addr(x_pos, y_pos);
	// set the cursor
	lcdSendCmd(base, addr);
	return 0;
}
int lcdBorrarPos(alt_dev *base, unsigned x_pos, unsigned y_pos)
{
	// boundary check
	if (x_pos > 39 || y_pos > 1 )
		return -1;

	// get address
	unsigned char addr = get_DDRAM_addr(x_pos, y_pos);
	// set cursor to dest point
	lcdSendCmd(base, addr);
	char a[] = " ";
	//send an empty char as erase (refer to the Character Generator ROM part of the Datasheet)
	IOWR_ALT_UP_CHARACTER_LCD_DATA(base, a );
	return 0;
}
void lcdBorrarTodo(alt_dev *base){
	unsigned int i;
	unsigned int j;
	for (j=0; j<2;j++){
		for(i=0;i<=39;i++){
			lcdBorrarPos(base,i,j);
		}
	}

}



/*Ejemplo de uso

	char a []= "Que tal? LOCO!!!";
	char b [] = "Bien y tu?";

	lcdEscirbir(LCD_BASE, a);
	lcdSetCursor(LCD_BASE, 0,1);
	lcdEscirbir(LCD_BASE, b);
	getchar();
	getchar();
	lcdBorrarTodo(LCD_BASE);
	getchar();
	getchar();
	char c[] ="Allan ";
	lcdEscirbir(LCD_BASE, c);

	
	*/