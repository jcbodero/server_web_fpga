/*
 * "Hello World" example.
 *
 * This example prints 'Hello from Nios II' to the STDOUT stream. It runs on
 * the Nios II 'standard', 'full_featured', 'fast', and 'low_cost' example
 * designs. It runs with or without the MicroC/OS-II RTOS and requires a STDOUT
 * device in your system's hardware.
 * The memory footprint of this hosted application is ~69 kbytes by default
 * using the standard reference design.
 *
 * For a reduced footprint version of this template, and an explanation of how
 * to reduce the memory footprint for a given application, see the
 * "small_hello_world" template.
 *
 */

#include <stdio.h>
#include "alt_types.h"

int main()
{
  printf("Hello from Nios II!\n");
  alt_u8 a = 0x2;
 	    	 alt_u8 b = 0x4;
 	    	 alt_u8 c = a & b;
 	    	 alt_u8 d = a | b;
 	    	 alt_u8 e = a | 0x8;
 	    	 alt_u8 f = 0x2;
 	    	 f = f | 0x4;
 	    	 printf("\n%d  %d  %d   %d\n",c,d,e,f );
  return 0;
}
