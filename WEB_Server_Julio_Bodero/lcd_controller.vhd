LIBRARY ieee;
USE ieee.std_logic_1164.ALL;

ENTITY lcd_controller IS
	GENERIC(CURSOR_ON: std_logic:='1';
				BLINKING_ON: std_logic:='1');
  PORT(
    clk        : IN    STD_LOGIC;  --system clock
    reset    : IN    STD_LOGIC;  --active low reinitializes lcd
    address, chipselect : in std_logic;
	 read : in std_logic;
	 write : in std_logic;
	 writedata : in std_logic_vector(7 downto 0); -- Data for the bottom line of the LCD
	 lcd_data   : INOUT   STD_LOGIC_VECTOR(7 DOWNTO 0); --data signals for lcd
	 lcd_on, lcd_blon, lcd_rw, lcd_rs, lcd_en, waitrequest  : OUT   STD_LOGIC;  --read/write, setup/data, and enable for lcd
	 readdata : out std_logic_vector(7 downto 0));	 
	 --line1_buffer : IN STD_LOGIC_VECTOR(127 downto 0); -- Data for the top line of the LCD
	 --line2_buffer : IN STD_LOGIC_VECTOR(127 downto 0);
END lcd_controller;

ARCHITECTURE controller OF lcd_controller IS
	COMPONENT lcd_communication IS
	GENERIC(CLOCK_CYCLES_FOR_IDLE_STATE: std_logic_vector(6 downto 0):="1111111";
				IC: integer:=7;
				IDLE_COUNTER_INCREMENT: std_logic_vector(6 downto 0):="0000001";
				CLOCK_CYCLES_FOR_OPERATION_STATE:integer:=3;
				CLOCK_CYCLES_FOR_ENABLE_STATE:integer:=15;
				CLOCK_CYCLES_FOR_HOLD_STATE:integer:=1;
				SC: integer:=4;
				COUNTER_INCREMENT: std_logic_vector(3 downto 0):="0001");
	  PORT(
		clk        : IN    STD_LOGIC;
		reset    : IN    STD_LOGIC;
		data_in: in std_logic_vector(7 downto 0);
		enable,rs,rw,display_on,back_light_on: in std_logic;
		LCD_DATA:inout std_logic_vector(7 downto 0);
		LCD_ON,LCD_BLON,LCD_EN,LCD_RS,LCD_RW,transfer_complete:out std_logic;
		data_out : out std_logic_vector(7 downto 0));
	END COMPONENT;
	COMPONENT lcd_initialization IS
		GENERIC(
		CURSOR_ON: std_logic:='1';
		BLINKING_ON: std_logic:='1';
		CLOCK_CYCLES_FOR_15MS: integer:=750000;
		W15: integer:=20;
		COUNTER_INCREMENT_FOR_15MS: std_logic_vector(19 downto 0):="00000000000000000001";
		CLOCK_CYCLES_FOR_5MS: integer:=250000;
		W5: integer:=18;
		COUNTER_INCREMENT_FOR_5MS: std_logic_vector(17 downto 0):="000000000000000001");
		PORT(
		 clk        : IN    STD_LOGIC;
		 reset    : IN    STD_LOGIC;
		 initialize_LCD_display, command_was_sent: in std_logic;
		 done_initialization, send_command: out std_logic;
		 the_command: out std_logic_vector(8 downto 0));
	END COMPONENT;
	CONSTANT LCD_STATE_0_IDLE: std_logic_vector(2 downto 0):="000";
	CONSTANT LCD_STATE_1_INITIALIZE: std_logic_vector(2 downto 0):="001";
	CONSTANT LCD_STATE_2_START_CHECK_BUSY: std_logic_vector(2 downto 0):="010";
	CONSTANT LCD_STATE_3_CHECK_BUSY: std_logic_vector(2 downto 0):="011";
	CONSTANT LCD_STATE_4_BEGIN_TRANSFER: std_logic_vector(2 downto 0):="100";
	CONSTANT LCD_STATE_5_TRANSFER: std_logic_vector(2 downto 0):="101";
	CONSTANT LCD_STATE_6_COMPLETE: std_logic_vector(2 downto 0):="110";
	SIGNAL transfer_complete: std_logic;
	SIGNAL done_initialization: std_logic;
	SIGNAL init_send_command: std_logic;
	SIGNAL init_command: std_logic_vector(8 downto 0);
	SIGNAL send_data: std_logic;
	SIGNAL data_received: std_logic_vector(7 downto 0);
	SIGNAL initialize_lcd_display: std_logic;
	SIGNAL data_to_send:  std_logic_vector(7 downto 0);
	SIGNAL rs: std_logic;
	SIGNAL rw: std_logic;
	SIGNAL ns_lcd_controller: std_logic_vector(2 downto 0);
	SIGNAL s_lcd_controller: std_logic_vector(2 downto 0);
	SIGNAL lcd_on_x1: std_logic;
	SIGNAL lcd_blon_x2: std_logic;
	SIGNAL lcd_en_x3: std_logic;
	SIGNAL lcd_rs_x4: std_logic;
	SIGNAL lcd_rw_x5: std_logic;
	SIGNAL readdata_x6: std_logic_vector(7 downto 0);
	SIGNAL waitrequest_x7: std_logic;
	SIGNAL temp_X8: std_logic;
	SIGNAL temp_x9: std_logic;
	SIGNAL temp_x10: std_logic;
	SIGNAL asig_wait: std_logic;
BEGIN
	lcd_on <= lcd_on_x1;
	lcd_blon <= lcd_blon_x2;
	lcd_en <= lcd_en_x3;
	lcd_rs <= lcd_rs_x4;
	lcd_rw <= lcd_rw_x5;
	readdata <= readdata_x6;
	waitrequest <= waitrequest_x7;
	process
	begin
		wait until (clk'event and clk='1');
		if(reset='1') then
			s_lcd_controller<=LCD_STATE_0_IDLE;
		else
			s_lcd_controller<=ns_lcd_controller;
		end if;
	end process;
	process(chipselect, ns_lcd_controller, transfer_complete, initialize_lcd_display,
				 s_lcd_controller, clk, reset, data_received,  done_initialization)
	begin
		ns_lcd_controller<=LCD_STATE_0_IDLE;
		case s_lcd_controller is
		when LCD_STATE_0_IDLE=>if(initialize_lcd_display='1') then ns_lcd_controller<=LCD_STATE_1_INITIALIZE;
										else if(chipselect='1') then ns_lcd_controller<=LCD_STATE_2_START_CHECK_BUSY;
										else ns_lcd_controller<=LCD_STATE_0_IDLE; end if; end if;
		when LCD_STATE_1_INITIALIZE=>if(done_initialization='1') then ns_lcd_controller<=LCD_STATE_6_COMPLETE;
										else ns_lcd_controller<=LCD_STATE_1_INITIALIZE; end if;
		when LCD_STATE_2_START_CHECK_BUSY=>if(transfer_complete='0') then ns_lcd_controller<=LCD_STATE_3_CHECK_BUSY;
										else ns_lcd_controller<=LCD_STATE_2_START_CHECK_BUSY; end if;
		when LCD_STATE_3_CHECK_BUSY=>if(((transfer_complete) and (data_received(7)))='1') then ns_lcd_controller<=LCD_STATE_2_START_CHECK_BUSY;
										else if(((transfer_complete='1') and (data_received(7)='0'))) then ns_lcd_controller<=LCD_STATE_4_BEGIN_TRANSFER;
										else ns_lcd_controller<=LCD_STATE_3_CHECK_BUSY; end if; end if;
		when LCD_STATE_4_BEGIN_TRANSFER=>if(transfer_complete='0') then ns_lcd_controller<=LCD_STATE_5_TRANSFER;
										else ns_lcd_controller<=LCD_STATE_4_BEGIN_TRANSFER; end if;
		when LCD_STATE_5_TRANSFER=>if(transfer_complete='1') then ns_lcd_controller<=LCD_STATE_6_COMPLETE;
										else ns_lcd_controller<=LCD_STATE_5_TRANSFER; end if;
		when LCD_STATE_6_COMPLETE=> ns_lcd_controller<=LCD_STATE_0_IDLE;
		when OTHERS => ns_lcd_controller<=LCD_STATE_0_IDLE;
	end case;
	end process;
	
	process
	begin
	wait until(clk'event and clk='1');
	if(reset='1') then
		initialize_lcd_display<='1';
	else if(done_initialization='1') then initialize_lcd_display<='0';
			end if; end if;
	end process;
	
	process
	begin
	wait until(clk'event and clk='1');
	if(reset='1') then
		data_to_send<="00000000";
	else if(s_lcd_controller = LCD_STATE_1_INITIALIZE) then data_to_send<=init_command(7 downto 0);
		else if (s_lcd_controller = LCD_STATE_4_BEGIN_TRANSFER) then data_to_send<=writedata(7 downto 0);
		end if;end if;end if;
	end process;

	process
	begin
	wait until(clk'event and clk='1');
	if(reset='1') then rs<='0';
	else if(s_lcd_controller=LCD_STATE_1_INITIALIZE) then rs<=init_command(8);
		else if(s_lcd_controller=LCD_STATE_2_START_CHECK_BUSY) then rs<='0';
		else if(s_lcd_controller=LCD_STATE_4_BEGIN_TRANSFER) then rs<=address;
		end if;end if;end if;end if;
	end process;
	
	process
	begin
	wait until(clk'event and clk='1');
	if(reset='1') then rw<='0';
	else if(s_lcd_controller=LCD_STATE_1_INITIALIZE) then rw<='0';
	else if(s_lcd_controller=LCD_STATE_2_START_CHECK_BUSY) then rw<='1';
	else if(s_lcd_controller=LCD_STATE_4_BEGIN_TRANSFER) then rw<=not write;
	end if;end if;end if;end if;
	end process;
	asig_wait<='0' when s_lcd_controller=LCD_STATE_6_COMPLETE else '1';
	readdata_x6<=data_received;
	waitrequest_x7<=chipselect and asig_wait;
	temp_x8<='1' when(s_lcd_controller=LCD_STATE_5_TRANSFER) else '0';
	temp_x9<='1' when(s_lcd_controller=LCD_STATE_3_CHECK_BUSY) else temp_x8;
	temp_x10<=init_send_command when(s_lcd_controller=LCD_STATE_1_INITIALIZE) else temp_x9;
	send_data<=temp_x10;
	
	Char_LCD_Comm: lcd_communication
	PORT MAP(clk=>clk,reset=>reset,data_in=>data_to_send,enable=>send_data,rs=>rs,
				rw=>rw,display_on=>'1',back_light_on=>'1',LCD_DATA=>LCD_DATA,
				LCD_ON=>LCD_ON_x1,LCD_BLON=>LCD_BLON_x2,LCD_EN=>LCD_EN_x3,
				LCD_RS=>LCD_RS_x4,LCD_RW=>LCD_RW_x5,data_out=>data_received,
				transfer_complete=>transfer_complete);	
	
	Char_LCD_Init: lcd_initialization
	Generic map (BLINKING_ON=>(BLINKING_ON),
					 CURSOR_ON=>(CURSOR_ON))
	PORT MAP(clk=>clk,reset=>reset,initialize_LCD_display=>initialize_lcd_display,
				command_was_sent=>transfer_complete, done_initialization=>done_initialization,
				send_command=>init_send_command,the_command=>init_command);
	
END controller;
