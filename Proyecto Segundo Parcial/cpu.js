let seconds = [];

for (let i = 0; i < 100; i++) seconds.push(i * 1);

let labels = [];

let cpuValue = [];

seconds.forEach(s => {
  labels.push(s + 's');
});

let infoRendimiento = {};

var speedCanvas = document.getElementById("cpuChart");

Chart.defaults.global.defaultFontFamily = "Lato";
Chart.defaults.global.defaultFontSize = 18;

var speedData = {
  labels: labels,
  datasets: [{
    label: "Consumo del Cpu en KB/s",
    data: cpuValue,
    lineTension: 0,
    fill: false,
    borderColor: 'orange',
    backgroundColor: 'transparent',
    borderDash: [5, 5],
    pointBorderColor: 'orange',
    pointBackgroundColor: 'rgba(255,150,0,0.5)',
    pointRadius: 5,
    pointHoverRadius: 10,
    pointHitRadius: 30,
    pointBorderWidth: 2,
    pointStyle: 'rectRounded'
  }]
};

var chartOptions = {
  legend: {
    display: true,
    position: 'top',
    labels: {
      boxWidth: 80,
      fontColor: 'black'
    }
  }
};

let lineChart;

function getCpuUsage(){

  console.log("sfasfasf");

  fetch('http://192.168.1.3/file.json')
    .then(res => res.json())
    .then(data => {
      infoRendimiento = data;
    })

  cpuValue.push(infoRendimiento.Cpu);

  speedData.data = cpuValue;

  lineChart = new Chart(speedCanvas, {
    type: 'line',
    data: speedData,
    options: chartOptions
  });
}

setTimeout(getCpuUsage, 1000);
setInterval(getCpuUsage, 2000);


