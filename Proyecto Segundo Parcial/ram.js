let ramSeconds = [];

for (let i = 0; i < 100; i++) ramSeconds.push(i * 1);

let ramLabel = [];

let ramValue = [];

ramSeconds.forEach(s => {
  ramLabel.push(s + 's');
});

let ramInfoRendimiento = {};

let ramSpeedCanvas = document.getElementById("ramChart");

Chart.defaults.global.defaultFontFamily = "Lato";
Chart.defaults.global.defaultFontSize = 18;

let ramSpeedData = {
  labels: ramLabel,
  datasets: [{
    label: "Consumo de RAM en MB/s",
    data: ramValue,
    lineTension: 0,
    fill: false,
    borderColor: 'orange',
    backgroundColor: 'transparent',
    borderDash: [5, 5],
    pointBorderColor: 'orange',
    pointBackgroundColor: 'rgba(255,150,0,0.5)',
    pointRadius: 5,
    pointHoverRadius: 10,
    pointHitRadius: 30,
    pointBorderWidth: 2,
    pointStyle: 'rectRounded'
  }]
};

let ramChartOptions = {
  legend: {
    display: true,
    position: 'top',
    ramLabel: {
      boxWidth: 80,
      fontColor: 'black'
    }
  }
};

let ramLineChart;

function getRamUsage(){

  fetch('http://192.168.1.3/file.json')
    .then(res => res.json())
    .then(data => {
      ramInfoRendimiento = data;
    })

  ramValue.push(ramInfoRendimiento.Memoria);

  ramSpeedData.data = ramValue;

  ramLineChart = new Chart(ramSpeedCanvas, {
    type: 'line',
    data: ramSpeedData,
    options: ramChartOptions
  });
}

setTimeout(getRamUsage, 1000);
setInterval(getRamUsage, 2000);


